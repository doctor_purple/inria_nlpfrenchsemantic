class Word:
	""" This is the main class of words, contains the id numer, the word and the tag"""
	def __init__(self, id_word, word, tag):
		self.id_word = id_word
		self.word = word
		self.tag = tag
		self.crb_code = ''
		self.latitude = ''
		self.longitude = ''
	
	def toString(self):
		return self.id_word + " " + self.word + " " + self.tag
