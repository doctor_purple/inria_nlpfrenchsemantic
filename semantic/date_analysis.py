from tools import globalfile
from tools import normalise_text
import locale
import calendar
import unidecode
import datetime

 
months_list = ["janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"]
days_list = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
timeExprssion_list = ["aujourd'hui", "demain"]
prochain = ["prochain", "prochaine"]
month_number = ""
day_number = ""
days_to_add = 0


"""Handle the date extraction and parsing"""
def dateAnalysis(words_list, dates_list):
	locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
	days_to_add = 0
	
	month_number =  containsMonthName(dates_list)
	day_number = containsDayNumber(dates_list)

	if (not month_number or not day_number):
		if (dates_list and words_list):
			weekDay, prochaine_semaine = containsDayName(dates_list, words_list)
			if (weekDay):
				days_to_add = differenceOfDays(weekDay)
				if (prochaine_semaine is True):
					days_to_add += 7 #adding 7 if we are in the case of the next week

		elif (words_list):
			timeExpression = containsTimeExpression(words_list)
			if (timeExpression):
				days_to_add = timeExpression

		
		return addDaysToToday(days_to_add)
	
	elif (month_number and day_number):
		return dateCreation(int(day_number), month_number)	




#------------------------------------------------------------------------------------------------			
def containsMonthName(dates_list):
	for date_element in dates_list:
		if date_element.tag == globalfile.DATE_arto_TAG:
			date_elements_no_accents = normalise_text.removeAccents(date_element.word).lower()
			if date_elements_no_accents in months_list:
				return months_list.index(date_elements_no_accents) + 1

def containsDayNumber(dates_list):
	for date_element in dates_list:
		if date_element.tag == globalfile.DATE_arto_TAG:
			if date_element.word.isdigit():
				return date_element.word

def containsDayName(dates_list, words_list):
	prochaine_semaine = False
	for date_element in dates_list:
		if date_element.tag == globalfile.DATE_artf_TAG:
			if date_element.word.lower() in days_list:
				previous_word = next((current_word for current_word in words_list if current_word.id_word == str(int(date_element.id_word) - 1)), None)
				weekDay = days_list.index(date_element.word.lower())

				if (previous_word):
					if (previous_word.word.lower() in prochain):
						prochaine_semaine = True

				return weekDay, prochaine_semaine

def containsTimeExpression(words_list):
	days_to_add = 0
	for word_element in words_list:
		if word_element.word.lower() in timeExprssion_list:
			if word_element.tag.lower() == 'demain':
				#check the previous word
				previous_word = next((current_word for current_word in words_list if current_word.id_word == str(int(word_element.id_word) - 1)), None)
				if (previous_word):
					if previous_word.tag.lower() == 'apres':
						days_to_add = 2
					else:
						days_to_add = 1
	return days_to_add



#------------------------------------------------------------------------------------------------
def differenceOfDays(weekDay):
	today_dayOfWeek = datetime.datetime.today().weekday()
	

	#get the difference in days between the day name extracted and today
	if weekDay > today_dayOfWeek:
		days_to_add = weekDay - today_dayOfWeek
	else:
		days_to_add = abs(weekDay - today_dayOfWeek)
		days_to_add = 7 - days_to_add

	return days_to_add


def addDaysToToday(days_to_add):
	today = datetime.datetime.now()
	end_date = today + datetime.timedelta(days=days_to_add)
	return end_date.strftime("%Y-%m-%d")

def dateCreation(day, month):
	today = datetime.datetime.now()
	if (day > today.day and month >= today.month):
		#same year of today
		year = today.year
	else:
		year = today.year + 1

	date = datetime.date(year, month, day)
	return date.strftime("%Y-%m-%d")

			