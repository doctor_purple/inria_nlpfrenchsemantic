import urllib
import urllib2
import simplejson as json
import difflib
from tools import globalfile


"""" Handles the main loop"""
def locationAnalysis(words_list, locations_list):
	locations_list_updated = checkLocationWithCRB(locations_list)
	return checkOriginDestination( words_list, locations_list_updated)
#---------------------------------------------------------------------------
#which is really a location? double check with coordinates, and name present in our DB 
def checkLocationWithCRB(locations_list):
	url = 'http://ncetsplnx46:8095/CRBConnector/crb/suggest'
	locations_list_updated = locations_list[:] #copy the list, because i cannot remove elements when iterating

	for current_word in locations_list:
		value = {'query' : current_word.word}
		params = urllib.urlencode(value)
		full_url = url + '?' + params
		obj = urllib2.urlopen(full_url).read()
		data = json.loads(obj)
	 	if (len(data) == 0):
			locations_list_updated.remove(current_word)
		else:
			if (data):
				name = data[0]['name']
				crb_code = data[0]['id']
				latitude = data[0]['lat']
				longitude = data[0]['lng']

				string1 = current_word.word.lower()
				string2 = name.lower()
				
				similarity = similarityMeasure(string1, string2)
				if (similarity < 0.85):
					locations_list_updated.remove(current_word)
				else:
					current_word.crb_code = crb_code
					current_word.latitude = latitude
					current_word.longitude = longitude 

	return locations_list_updated


#check which is the previous word:
def checkOriginDestination(words_list, locations_list):
	#built the json object
	origin = {}
	destination = {}

	for current_word in locations_list:
		current_id = current_word.id_word
		previous_word = next((current_word for current_word in words_list if current_word.id_word == str(int(current_id) - 1)), None)
		if previous_word:
			if isOrigin(current_word, previous_word, locations_list):
				origin = {globalfile.TEXT_TAG:current_word.word, globalfile.ID_TAG:current_word.crb_code, globalfile.LATITUDE_TAG:current_word.latitude,globalfile.LONGITUDE_TAG:current_word.longitude}
			if isDestination(current_word, previous_word, locations_list):
				destination = {globalfile.TEXT_TAG:current_word.word, globalfile.ID_TAG:current_word.crb_code, globalfile.LATITUDE_TAG:current_word.latitude,globalfile.LONGITUDE_TAG:current_word.longitude}
	return origin, destination




#---------------------------------------------TOOL METHODS-------------------------------------------------
def similarityMeasure(string1, string2):
	#check if there spaces and take only the first split string
	if " " in string1:
		string1 = string1.split(" ")[0]
	if " " in string2:
		string2 = string2.split(" ")[0]

	similarity = difflib.SequenceMatcher(None, string1, string2).ratio()
	return similarity

def isOrigin (current_word, previous_word, locations_list):
	origin_list =["De", "Depuis"]
	# next_location = next((next_location for next_location in locations_list if next_location.id_word == str(int(current_word.id_word) + 1)), None)
	# if next_location: 
	# 	print next_location.word
	
	if previous_word.word in origin_list:
		return True
	# else:
	# 	if (next_location):
	# 		return True

def isDestination(current_word, previous_word, locations_list):
	destination_list = ["A", "Pour"]
	previous_location = next((previous_location for previous_location in locations_list if previous_location.id_word == str(int(current_word.id_word) - 1)), None)
	if previous_word.word in destination_list:
		return True
	# else:
	# 	if (previous_location is not None):
	# 		return True
