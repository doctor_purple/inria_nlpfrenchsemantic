#coding:UTF-8

import re
import unidecode
from lib.guess_language import guessLanguage



def capitalize_words(text):
	return re.sub(r"[A-Za-z]+('[A-Za-z]+)?", 
		lambda mo: mo.group(0)[0].upper() + mo.group(0)[1:].lower(), text)

def isFrench(text):
	language_code_french = "fr"
	if (guessLanguage(text) == language_code_french):
		return True
	else:
		return False

def removeAccents(text):
	return unidecode.unidecode(unicode(text, 'utf-8'))