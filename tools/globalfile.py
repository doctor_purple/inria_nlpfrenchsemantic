LOCATION_TAG = None
NUMBER_TAG = None
DATE_TAG = None

def init():
	global LOCATION_TAG, NUMBER_TAG, DATE_TAG, DATE_arto_TAG, DATE_artf_TAG,TEXT_TAG, ID_TAG, LATITUDE_TAG, LONGITUDE_TAG
	LOCATION_TAG = '_LOCATION'
	NUMBER_TAG = '_NUMBER'
	DATE_TAG = '_DATE'
	DATE_arto_TAG = '_DATE_arto'
	DATE_artf_TAG = '_DATE_artf'
	TEXT_TAG = 'text'
	ID_TAG = 'id'
	LATITUDE_TAG = 'latitude'
	LONGITUDE_TAG = 'longitude'
	