import re
from objects.word import Word
from tools import globalfile
globalfile.init()


def parse_UDAG(udag_text):
	words_list = []
	reg_brackets =  re.compile('^\d*.+\{(.*)\} (\w*).+\d$')
	reg = re.compile('\<F id="E\d*F(\d+)"\>([A-Za-z0-9]+)\<\/F\>')
	

	for line in udag_text.splitlines():
		brackets_match = reg_brackets.match(line)
		if brackets_match:
			brackets_text = brackets_match.groups()[0]
			tag = brackets_match.groups()[1]
			brackets_text_matches = re.findall(reg, brackets_text)

			for single_match in brackets_text_matches:
				id_word = single_match[0]
				word = single_match[1]
				current_word = Word(id_word, word, tag)
				words_list.append(current_word)
				#print current_word.toString()
	return words_list
	


#---------------------------------------------PRIVATE METHODS---------------------------------------
#remove duplicates
def build_arrays(words_list):
	locations_list = []
	numbers_list = []
	dates_list = []
	for current_word in words_list:
		if (current_word.tag == globalfile.LOCATION_TAG):
			if not [current_location for current_location in locations_list if current_word.id_word == current_location.id_word]:
				locations_list.append(current_word)
		elif (current_word.tag == globalfile.NUMBER_TAG):
			if not [current_number for current_number in numbers_list if current_word.id_word == current_number.id_word]:
				numbers_list.append(current_word)
		elif (globalfile.DATE_TAG in current_word.tag ):
			if not [current_date for current_date in dates_list if current_word.id_word == current_date.id_word]:
				dates_list.append(current_word)

	return locations_list, numbers_list, dates_list




			


			






			
			

			


			

			

			
		
