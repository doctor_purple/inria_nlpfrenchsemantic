#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-


import subprocess
import simplejson as json

from tools import normalise_text 
from tools import output_parser
from semantic import location_analysis
from semantic import date_analysis




def main():
	results = {}

	while(True):
		text = raw_input("Enter your sentence in French: ")
		#----------------------------------------------------------------------------------------------------------------------------
		#call an external class, able to detect the language and put the capital letters of each words
		text_capitalized = normalise_text.capitalize_words(text)
		#I call the sxpipe-easy commands in the udag format, in order to use the same structure XML for the next steps	
		proc = subprocess.Popen('echo "%s" | sxpipe-afp | dag2udag' %text_capitalized, shell=True, stdout=subprocess.PIPE)
		output, error = proc.communicate()
		print output #this prints the output xml dag format returned by SxPipe
		exitCode = proc.returncode

		
		#print the result
		if (exitCode == 0):
			#output parser: take the udag format and transform in my structure
			words_list = output_parser.parse_UDAG(output)
			locations_list, numbers_list, dates_list = output_parser.build_arrays(words_list)
			#with the arrays I can call the methods of the semantic analyzer
			origin, destination = location_analysis.locationAnalysis(words_list, locations_list)
			date = date_analysis.dateAnalysis(words_list, dates_list)

			
			results = {'origin':origin, 'destination':destination, 'departure':date, 'tickets': '1', 'tickets_class':'2'} #create the python dictionary
			results = json.dumps(results)
			print results





if __name__ == "__main__":
	main()

