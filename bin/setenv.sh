## Source this file or add it to your .bash_profile

## Edit following variable
export ALPAGE_PREFIX="/home/alle/exportbuild"

## Following variables should be ok
#export LC_ALL=fr_FR
export LD_RUN_PATH="$ALPAGE_PREFIX/lib/:$ALPAGE_PREFIX/lib/DyALog/:$ALPAGE_PREFIX/lib/DyALog/dyalog-xml:$ALPAGE_PREFIX/lib/DyALog/dyalog-sqlite:$LD_RUN_PATH:/usr/local/lib:/usr/lib"
export LD_LIBRARY_PATH="$ALPAGE_PREFIX/lib/:$ALPAGE_PREFIX/lib/DyALog/:$ALPAGE_PREFIX/lib/DyALog/dyalog-xml:$ALPAGE_PREFIX/lib/DyALog/dyalog-sqlite:$LD_LIBRARY_PATH:/usr/local/lib:/usr/lib"
export LIBRARY_PATH="$ALPAGE_PREFIX/lib/:$ALPAGE_PREFIX/lib/DyALog/:$ALPAGE_PREFIX/lib/DyALog/dyalog-xml:$ALPAGE_PREFIX/lib/DyALog/dyalog-sqlite:$LIBRARY_PATH:/usr/local/lib:/usr/lib"
export PATH="$ALPAGE_PREFIX/bin/:$ALPAGE_PREFIX/share/frmg/:$ALPAGE_PREFIX/share/biomg/:$ALPAGE_PREFIX/sbin/:$PATH:/usr/local/bin:/usr/bin"
export LDFLAGS="-L${ALPAGE_PREFIX}/lib $LDFLAGS"
export PERL5LIB="${ALPAGE_PREFIX}/lib/perl5/site_perl:${ALPAGE_PREFIX}/lib/perl5:${ALPAGE_PREFIX}/lib/perl5/site_perl/5.14.2:${ALPAGE_PREFIX}/lib/perl/5.14.2/auto:${ALPAGE_PREFIX}/share/automake-1.10:${ALPAGE_PREFIX}/share/perl/5.14.2:${ALPAGE_PREFIX}/lib/perl/5.14.2:${ALPAGE_PREFIX}/lib/perl:${ALPAGE_PREFIX}/lib/perl/5.8:${ALPAGE_PREFIX}/share/perl/5.8:$PERL5LIB:/usr/lib/perl5/vendor_perl/5.8.8"
export PKG_CONFIG_PATH="${ALPAGE_PREFIX}/lib/pkgconfig:$PKG_CONFIG_PATH"

## for perl and cpan
export PERL_MM_OPT="INSTALL_BASE=${ALPAGE_PREFIX} INSTALLSITEBIN=${ALPAGE_PREFIX}/bin INSTALLSITESCRIPT=${ALPAGE_PREFIX}/bin INSTALLSITEMAN1DIR=${ALPAGE_PREFIX}/man/man1 INSTALLSITEMAN3DIR==${ALPAGE_PREFIX}/man/man3"
export MODULEBUILDRC="${ALPAGE_PREFIX}/lib/perl5/.modulebuildrc"

## for python and easy_install
export PYTHONPATH="${ALPAGE_PREFIX}/lib:$PYTHONPATH"

## for MacOS
export DYLD_LIBRARY_PATH="$DYLD_LIBRARY_PATH:$ALPAGE_PREFIX/lib/:$ALPAGE_PREFIX/lib/DyALog/:$ALPAGE_PREFIX/lib/DyALog/dyalog-xml:$ALPAGE_PREFIX/lib/DyALog/dyalog-sqlite"

